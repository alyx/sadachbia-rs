use actix_web::{get, post, web, HttpResponse, Responder};
use tera::Context;

#[get("/")]
async fn index(data: web::Data<crate::State>) -> impl Responder {
    let ctx = Context::new();
    let resp = data.tera.render("pages/index.html", &ctx);
    HttpResponse::Ok().body(resp.unwrap())
}

#[post("/")]
pub async fn submit(data: web::Data<crate::State>, _form: web::Form<crate::ShortForm>) -> HttpResponse {
    let mut ctx = Context::new();
    let id = data.hash.next_id();
    ctx.insert("url", &id);
    let resp = data.tera.render("pages/index.html", &ctx);
    HttpResponse::Ok().body(resp.unwrap())
}