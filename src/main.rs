use actix_web::{middleware, App, HttpServer};
use actix_files as fs;
use tera::Tera;
use serde::Deserialize;

mod shortener;
mod rewrite;

pub struct State {
    tera: Tera,
    hash: mut shortener::Shortener,
}

#[derive(Deserialize)]
pub struct ShortForm {
    url: String,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    println!("\u{1F98A} Server running on :8088");

    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    HttpServer::new(|| {
        App::new()
            .wrap(middleware::Logger::default())
            .data(State{
                tera: match Tera::new("web/templates/**/*.html") {
                    Ok(t) => t,
                    Err(e) => {
                        println!("Parsing error(s): {}", e);
                        ::std::process::exit(1);
                    }
                },
                hash: shortener::Shortener::new(),

            })
            .service(fs::Files::new("/static", "./web/static"))
            .service(rewrite::index)
            .service(rewrite::submit)
    })
    .bind("127.0.0.1:8088")?
    .run()
    .await
}